# -*- coding: utf-8 -*-
"""# Import modules"""

# import modules and libraries
import torch
import PIL
import pandas as pd
import random
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import numpy as np
import torchvision
import os

from PIL import ImageFile

from math import sin, cos
from torch_snippets import Report

import torchvision.transforms.functional as functional
import torchvision.transforms.transforms as transforms

from torchvision.models.detection import fasterrcnn_resnet50_fpn
from torchvision.models.detection.faster_rcnn import FastRCNNPredictor

ImageFile.LOAD_TRUNCATED_IMAGES = True

"""# Load Data"""

# load annotations

csv_path = r'./annotations/annotations.csv'
df_annotations = pd.read_csv(csv_path, sep=',')


# converter csv annotation to numpy array

def csv_to_numpy(num_img=1):
    name_img = str(num_img) + '.jpg'
    filepath = os.path.join(r'./images', name_img)
    # get the boxes and label of it
    boxes = df_annotations[['xmin', 'ymin', 'xmax', 'ymax']].loc[df_annotations['filename'] == name_img].to_numpy()
    labels = df_annotations['class'].loc[df_annotations['filename'] == name_img].to_numpy()
    # conversion label to int
    labels[labels == 'waldo'] = 1
    labels = labels.astype('int64')
    return boxes, labels


class WaldoDataset(torch.utils.data.Dataset):
    def __init__(self, path, transforms=None):
        """
        :param path: str
          path to the data folder.
        :param transforms: Compose or list
          Torchvision image transformations.
        """
        self.path = path
        self.transforms = transforms
        self.files = sorted(os.listdir("images"))
        for i in range(len(self.files)):
            if self.files[i].split(".")[0] != '':
                self.files[i] = self.files[i].split(".")[0]

    def __getitem__(self, i):
        # load image
        img = PIL.Image.open(os.path.join(self.path, r'./images/' + str(i)
                                          + ".jpg")).convert("RGB")
        # load boxes and labels
        boxes, labels = csv_to_numpy(num_img=i)
        # construct target wich contains boxes, labels and the num of image
        target = dict()
        target["boxes"] = torch.from_numpy(boxes)
        target["labels"] = torch.from_numpy(labels)
        target["image_id"] = torch.as_tensor(i)
        # apply potential transforms to image and target
        if self.transforms is not None:
            img, target = self.transforms(img, target)
        return img, target

    def __len__(self):
        return len(self.files)


"""# Data Augmentation"""


class Compose:
    """
    Combine several torchvision image transforms
    """

    def __init__(self, transforms=[]):
        """
        :params transforms: List
            List of image transformations from torchvision
        """
        self.transforms = transforms

    def __call__(self, image, target):
        """
        Apply the torchvision transformation to the image and return the new
        image and target
        :params image: PIL Image
        :params target: dict
          dictionary with boxes and label
        :returns: modified image and target
        """
        for t in self.transforms:
            image, target = t(image, target)
        return image, target


class ToTensor(torch.nn.Module):
    """
    Converts a PIL image into a torch tensor.
    """

    @staticmethod
    def forward(image, target=None):
        """
        :params image: PIL Image
        :params target: dict
        :returns: tensor image with the same target
        """
        image = functional.pil_to_tensor(image)
        image = functional.convert_image_dtype(image)
        return image, target


class RandomHorizontalFlip(transforms.RandomHorizontalFlip):
    """
    Flips an image horizontally
    """

    def forward(self, image, target=None):
        """
        :params image: tensor
        :params target: dict
        :returns: horizontally flipped image and target flipped
        """
        if torch.rand(1) < self.p:
            image = functional.hflip(image)
            if target is not None:
                width, _ = functional.get_image_size(image)
                target["boxes"][:, [0, 2]] = width - target["boxes"][:, [2, 0]]
        return image, target


class RandomVerticalFlip(transforms.RandomVerticalFlip):
    """
    Flips an image vertically
    """

    def forward(self, image, target=None):
        """
        :params image: tensor
        :params target: dict
        :returns: vertically flipped image and target flipped
        """
        if torch.rand(1) < self.p:
            image = functional.hflip(image)
            if target is not None:
                _, height = functional.get_image_size(image)
                target["boxes"][:, [1, 3]] = height - target["boxes"][:, [3, 1]]
        return image, target


class RandomRotation(transforms.RandomRotation):
    """
    Random rotation of an image
    """

    def forward(self, image, target=None):
        """
        :params image: tensor
        :params target: dict
        :returns: rotated  image and target with box larger including rotated
        box
        """
        if torch.rand(1) < 0.25:
            theta = random.uniform(0, 180.)
            image = functional.rotate(image, angle=theta)

            if target is not None:
                for j in range(target["boxes"].size()[0]):
                    w = target["boxes"][j, [2]].item() - target["boxes"][j, [0]].item()
                    h = target["boxes"][j, [3]].item() - target["boxes"][j, [1]].item()

                    nW = int((h * sin(theta)) + (w * cos(theta)))
                    nH = int((h * cos(theta)) + (w * sin(theta)))

                    x_pt1 = int(target["boxes"][j, 0] - int((nW - w) / 2))
                    x_pt2 = int(x_pt1 + int(nW))
                    y_pt1 = int(target["boxes"][j, 1] - int((nH - h) / 2))
                    y_pt2 = int(y_pt1 + int(nH))

                    target["boxes"][j, 0] = min(x_pt1, x_pt2)
                    target["boxes"][j, 2] = max(x_pt1, x_pt2) + 1
                    target["boxes"][j, 1] = min(y_pt1, y_pt2)
                    target["boxes"][j, 3] = max(y_pt1, y_pt2) + 1
        return image, target


class RandomGaussian(transforms.GaussianBlur):
    """
    Random gaussian blur of an image
    """

    def forward(self, image, target=None):
        """
        :params image: tensor
        :params target: dict
        :returns: gaussian blur applied to image and same target
        """
        if torch.rand(1) < 0.5:
            image = functional.gaussian_blur(image, kernel_size=(5, 5))
        return image, target


def get_transform(train):
    """
    Converts a PIL Image into a torch tensor, and apply transforms
    if it's for training the model.
    :param train: bool
        Flag indicating if apply transforms (if True apply transform)
    :return:
        Composition of image transforms.
    """
    transforms = [ToTensor()]

    if train is True:
        transforms.append(RandomHorizontalFlip(0.5))
        # transforms.append(RandomVerticalFlip(0.5))
        transforms.append(RandomRotation(0.5))
        transforms.append(RandomGaussian(7))
    return Compose(transforms)


"""# Create Dataset"""

# train dataset
train = WaldoDataset("./", get_transform(train=True))
# validation dataset
validation = WaldoDataset("./", get_transform(train=False))
# test dataset
test = WaldoDataset("./", get_transform(train=False))

# shuffle all the data
indices = torch.randperm(len(train)).tolist()
files = sorted(os.listdir("images"))
list_index = []
for f in files:
    list_index.append(int(f.split('.')[0]))
result = [list_index[i] for i in indices]

# train dataset (80% of dataset)
train = torch.utils.data.Subset(train, result[:int(len(indices) * 0.8)])
# validation dataset (10% of dataset)
validation = torch.utils.data.Subset(validation, result[int(len(indices) * 0.8):int(len(indices) * 0.9)])
# test dataset (10% of dataset)
test = torch.utils.data.Subset(test, result[int(len(indices) * 0.9):])


# DataLoader
def collate_fn(batch):
    return tuple(zip(*batch))


# create dataloaders from dataset
train_dloader = torch.utils.data.DataLoader(train, batch_size=8, shuffle=True, collate_fn=collate_fn)
validation_dloader = torch.utils.data.DataLoader(validation, batch_size=8, shuffle=False, collate_fn=collate_fn)
test_dloader = torch.utils.data.DataLoader(test, batch_size=8, shuffle=False, collate_fn=collate_fn)

"""# Train model"""


def get_model(num_classes=2, feature_extraction=True):
    """
    :params num_classes: int
        Number of classes to predict. two in our exemple because Waldo and the
        background
    :params feature_extraction: bool
        Flag for freezing the weights if True, else udapted during training
    :returns model: model of FasterRCNN trained
    """
    # load the pretrained faster r-cnn model
    model = fasterrcnn_resnet50_fpn(pretrained=True)
    # if true, model is freezed
    if feature_extraction is True:
        for p in model.parameters():
            p.requires_grad = False
    in_feats = model.roi_heads.box_predictor.cls_score.in_features
    model.roi_heads.box_predictor = FastRCNNPredictor(in_feats, num_classes)
    return model


def unbatch(batch, device, only_predict=False):
    """
    Unbatches a batch of data from the Dataloader
    :params batch: tuple
        tuple containing a batch from the Dataloader
    :params device: str
        Indicates which CPU pr GPU device is used
    :params only_predict: boolean
        True if we do not need the target (new image)
    :returns: X list of images and y list of dictionnaries if only predict is false
    """
    if type(batch) == type((0, 0)):
        X, y = batch
        X = [x.to(device) for x in X]
        y = [{k: v.to(device) for k, v in t.items()} for t in y]
        return X, y
    else:
        X = [x.to(device) for x in batch]
        return X


def train_batch(batch, model, optimizer, device):
    """
    Uses back propagation to train the model
    :params batch: tuple
        Tuple containing a batch from the Dataloader
    :params model: torch model
    :params optimizer: torch optimizer
    :params device: str
        Indicates which CPU pr GPU device is used
    :returns: loss wich is the sum of the batch losses and losses a dictionary
    containing the individual losses
    """
    model.train()
    X, y = unbatch(batch, device=device)
    optimizer.zero_grad()
    losses = model(X, y)
    loss = sum(loss for loss in losses.values())
    loss.backward()
    optimizer.step()
    return loss, losses


@torch.no_grad()
def validate_batch(batch, model, optimizer, device):
    """
    Evaluate the model loss with the validation batch
    :params batch: tuple
          Tuple containing a batch from the Dataloader
    :params model: torch model
    :params optimizer: torch optimizer
    :params device: str
          Indicates which CPU pr GPU device is used
    :returns: loss wich is the sum of the batch losses and losses a dictionary
    containing the individual losses
    """
    model.train()
    X, y = unbatch(batch, device=device)
    optimizer.zero_grad()
    losses = model(X, y)
    loss = sum(loss for loss in losses.values())
    return loss, losses


def train_fasterrcnn(model, optimizer, n_epochs, train_loader, test_loader=None, log=None, keys=None,
                     device="cpu"):
    """
    Trains a FasterRCNN model with train and validation DataLoaders
    Returns log wich contains a report on the training and validation losses.
    :params model: FasterRCNN
    :params optimizer: torch optimizer
    :params n_epoch: int
        Number of epochs to train.
    :params train_loader: DataLoader
    :params test_loader: DataLoader
    :params log: Record
        torch_snippet Record to record training progress
    :params keys: list
        list of differents fasterRCNN names
    :params device: str
        Indicates which CPU pr GPU device is used
    :returns: log, a torch_snippet containing all the training records
    """
    if log is None:
        log = Report(n_epochs)
    if keys is None:
        # all FasterRCNN loss names
        keys = ["loss_classifier", "loss_box_reg", "loss_objectness", "_rpn_box_reg"]
    model.to(device)
    for epoch in range(n_epochs):
        N = len(train_loader)
        for ix, batch in enumerate(train_loader):
            loss, losses = train_batch(batch, model,
                                       optimizer, device)
            step = epoch + (ix + 1) / N
            # save epoch and loss
            log.record(pos=step, trn_loss=loss.item(),
                       end="\r")
        if test_loader is not None:
            N = len(test_loader)
            for ix, batch in enumerate(test_loader):
                loss, losses = validate_batch(batch, model,
                                              optimizer, device)

                step = epoch + (ix + 1) / N
                # save epoch and loss
                log.record(pos=step, val_loss=loss.item(),
                           end="\r")
    log.report_avgs(epoch + 1)
    return log


# creation of the faster rcnn
model_rcnn = get_model(num_classes=2, feature_extraction=False)
# using the stochastic gradient descent
params = [p for p in model_rcnn.parameters() if p.requires_grad]
optimizer = torch.optim.SGD(params, lr=0.005, momentum=0.9, weight_decay=0.0005)
# Train the model over 100 epochs
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
log = train_fasterrcnn(model=model_rcnn, optimizer=optimizer, n_epochs=100, train_loader=train_dloader,
                       test_loader=validation_dloader, log=None, keys=None, device=device)

"""# Model Prediction"""


@torch.no_grad()
def batch_prediction(batch, model, device, only_predict=False):
    """
    prediction for a batch of data
    :params batch: tuple
    :params model: torch model
    :params device: str
        Indicates which CPU pr GPU device is used
    :params only_predict: boolean
        True if we do not need the target (new image)
    :returns: a list of tensors of the images and a list of dicts containing the
    predictions for the bounding boxes, labels and confidence scores
    """
    model.to(device)
    model.eval()
    if only_predict:
        images = unbatch(batch, device=device, only_predict=only_predict)
    else:
        images, _ = unbatch(batch, device=device, only_predict=only_predict)
    predictions = model(images)
    return [image.cpu() for image in images], predictions


def predict(model, data_loader, device="cpu", only_predict=False):
    """
    Gets the predictions for a batch of data.
    :params model: torch model
    :params data_loader: torch DataLoader
    :params device: str
        Indicates which CPU pr GPU device is used
    :params only_predict: bool
        True if we do not need the target (new image)
    :returns: images a list of images (tensors) and a list of dictionnaires
    containing the predictions for the bounding boxes, labels and confidence
    scores
    """
    images = []
    predictions = []
    for i, batch in enumerate(data_loader):
        batch_images, batch_predictions = batch_prediction(batch, model, device, only_predict)
        images = images + batch_images
        predictions = predictions + batch_predictions

    return images, predictions


def decode_prediction(prediction, score_threshold=0.15, nms_iou_threshold=0.1):
    """
    :params prediction: dict
    :params score_threshold: float
    :params prediction: tuple
    :returns: a tuple containing the boxes, labels and scores
    """
    boxes = prediction["boxes"]
    scores = prediction["scores"]
    labels = prediction["labels"]
    # removing low-score under the threshold score
    if score_threshold is not None:
        index_remaining = scores > score_threshold
        boxes = boxes[index_remaining]
        scores = scores[index_remaining]
        labels = labels[index_remaining]
    # removing overlapping bounding boxes using nms algorithm
    if nms_iou_threshold is not None:
        index_remaining = torchvision.ops.nms(boxes=boxes, scores=scores, iou_threshold=nms_iou_threshold)
        boxes = boxes[index_remaining]
        scores = scores[index_remaining]
        labels = labels[index_remaining]
    return boxes.cpu().detach().numpy(), labels.cpu().detach().numpy(), scores.cpu().detach().numpy()


images, predictions = predict(model_rcnn, test_dloader, device)

"""# Compute metrics"""

DICES = []

for k in range(len(predictions)):
    boxes, labels, scores = decode_prediction(predictions[k])

    boxes = boxes.astype(int)
    boxes_truth = np.array(test_dloader.dataset[k][1]['boxes'])

    # segmentation
    seg = np.zeros_like(images[k][0])
    if np.shape(boxes)[0] > 0:
        seg[boxes[0][0]:boxes[0][2], boxes[0][1]:boxes[0][3]] = 1.

    # ground truth
    gt = np.zeros_like(images[k][0])
    gt[boxes_truth[0][0]:boxes_truth[0][2], boxes_truth[0][1]:boxes_truth[0][3]] = 1.

    dice = np.sum(seg[gt == 1]) * 2.0 / (np.sum(seg) + np.sum(gt))
    DICES.append(dice)

print('Dice similarity : ', np.mean(DICES))

"""# Save model"""

torch.save(model_rcnn, 'model_rcnn')