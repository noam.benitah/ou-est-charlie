# detection_charlie

## installations nécessaires

pip install torch  
pip install torch-snippets

## exécution

Pour entraîner le faster r-cnn, appliquer sur le jeu de données de validation et calculer le dice coefficient :

```
python main.py
```

Pour exécuter l'algorithme sur une nouvelle image :

```
python test.py PATH_IMG
```
__L'image avec Charlie encadré sera placée dans le dossier output__

## Temps d'execution

Pour exécuter les algorithmes en temps raisonnable, ils ont été exécutés sur les GPU de Google Collab.  

Si le temps d'execution est trop élevé lorsque vous lancez le __main.py__ vous avez deux alternatives : 

1. Vous pouvez utiliser directement le modèle que nous avons entrainé avec 100 epochs en remplaçant dans __test.py__ la ligne 21 par : __model_rcnn = torch.load('model_train/100_epochs')__ puis en executant seulement : 
```
python test.py PATH_IMG
``` 

2. Vous pouvez modifier la ligne 417 de __main.py__ en réduisant n_epochs = 100 à n_epochs = 50 ou n_epochs = 25 par exemple. Cela aura bien sûr une conséquence sur la qualité de l'algorithme.

## Tests

Nous avons testé l'algorithme sur plusieurs images présentes dans le dossier images. Nous avons mis quelques résultats dans le dossier tests. Les résultats peuvent varier selon la taille de la foule et la qualité de l'image.