# import modules and libraries

import PIL
import sys
import torch
import torchvision

import numpy as np
import matplotlib.pyplot as plt

import torchvision.transforms.functional as functional
import matplotlib.patches as patches

# get image tested

sys_args = sys.argv
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

# get model

model_rcnn = torch.load('model_rcnn')

# decode prediction

def decode_prediction(prediction, score_threshold=0.15, nms_iou_threshold=0.1):
    """
    :params prediction: dict
    :params score_threshold: float
    :params prediction: tuple
    :returns: a tuple containing the boxes, labels and scores
    """
    boxes = prediction["boxes"]
    scores = prediction["scores"]
    labels = prediction["labels"]
    # removing low-score under the threshold score
    if score_threshold is not None:
        index_remaining = scores > score_threshold
        boxes = boxes[index_remaining]
        scores = scores[index_remaining]
        labels = labels[index_remaining]
    # removing overlapping bounding boxes using nms algorithm
    if nms_iou_threshold is not None:
        index_remaining = torchvision.ops.nms(boxes=boxes, scores=scores, iou_threshold=nms_iou_threshold)
        boxes = boxes[index_remaining]
        scores = scores[index_remaining]
        labels = labels[index_remaining]
    return boxes.cpu().detach().numpy(), labels.cpu().detach().numpy(), scores.cpu().detach().numpy()

"""# Apply on an input image"""

image_path = sys_args[1]
print('-------------------------')
print(image_path)
img = PIL.Image.open(image_path).convert("RGB")
image = functional.pil_to_tensor(img)
image = functional.convert_image_dtype(image)
image = image.unsqueeze_(1)
image = image.to(device)

model_rcnn.eval()
model_rcnn.to(device)
prediction = model_rcnn(image)

boxes, labels, scores = decode_prediction(prediction[0])
fig_input, ax_input = plt.subplots(figsize=[5, 5])
ax_input.imshow(np.asarray(img))
for i, box in enumerate(boxes):
    rect = patches.Rectangle(box[:2].astype(int), (box[2] - box[0]).astype(int),
                             (box[3] - box[1]).astype(int), linewidth=1,
                             edgecolor="r", facecolor="none")
    ax_input.add_patch(rect)
    ax_input.text(int(box[0]), int(box[1]) - 5, "{} : {:.3f}".format("Waldo", scores[i]),
                  color="r")

plt.savefig('output/image_result.jpg', dpi=600)